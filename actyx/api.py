#!/usr/bin/env python
"""
    actyx..api
    ~~~~~~~~~~

    :copyright: (c) 2015 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: JSON HTTP API handler
"""


import requests
import json
import datetime
import logging

from actyx.models import MachineBuffered


class ApiHandler(object):
    ''' Machine API Handler 
    
    encapsulates reading the actyx machine API.
    '''

    def getMachineFromJSON(machineData):
        ''' return Machine object from JSON string'''

        # transform types from str
        timestamp = datetime.datetime.strptime(machineData['timestamp'], "%Y-%m-%dT%H:%M:%S.%f")
        current   = int(machineData['current'])

        machine = MachineBuffered(current=current,
                current_alert=int(machineData['current_alert']),
                location=machineData['location'],
                name=machineData['name'],
                state=machineData['state'],
                timestamp=timestamp,
                machineType=machineData['type'],
                bufferTimedelta = 5*60)

        return machine


    def urlJoin(self, url):
        '''resolve relative url'''
        url = url.replace('$API_ROOT',self.rootUrl)
        return url
        
       
    def __init__(self):
        self.rootUrl = 'http://machinepark.actyx.io/api/v1'
        self.machineListUrl = ''
        self.envSensorUrl = ''

        logging.info("Fetching api root.")
        response = requests.get(url=self.rootUrl)
        api_root = json.loads(response.text)

        # update machines list url
        logging.info("Fetching machine list.")
        machineListUrl = api_root['machines_list']
        self.machineListUrl = self.urlJoin(machineListUrl)

        # update environment sensor url
        logging.info("Fetching evironment url.")
        envSensorUrl = api_root['env_sensor']
        self.envSensorUrl = self.urlJoin(envSensorUrl)


    def __str__(self):
        return '''
%s
rootUrl:        %s
envSensorUrl:   %s
machineListUrl: %s
machineUrls   " [list]
        ''' % (self.__class__, self.envSensorUrl, self.rootUrl, self.machineListUrl)


    def getMachines(self):
        ''' returns list of machine urls'''

        response = requests.get(url=self.machineListUrl)
        machineUrls = json.loads(response.text)
        machineUrls = [self.urlJoin(machine) for machine in machineUrls]
        return machineUrls


    def getMachine(self,machineUrl):
        ''' returns machine data '''

        logging.info("Fetching machine at '%s'" % machineUrl)
        response = requests.get(url=machineUrl)
        machineData = json.loads(response.text)
        machine = ApiHandler.getMachineFromJSON(machineData)
        machine.url = machineUrl
        return machine


    def getEnvironment(self):
        ''' return environment sensor data '''

        logging.info("Fetching environment sensor data")
        response = requests.get(url=self.envSensorUrl)
        envData = json.loads(response.text)
        return envData
 

