#!/usr/bin/env python
"""
    actyx.machines
    ~~~~~~~~~~~~~~

    :copyright: (c) 2015 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: data objects
"""


import datetime
import logging

class Machine(object):
    ''' Machine data object '''

    def __init__(self, current=None, current_alert=None,\
            location=None, name=None,state=None, timestamp=None,\
            machineType=None, url=None):
        self.current       = current
        self.current_alert = current_alert
        self.location      = location
        self.name          = name
        self.state         = state
        self.timestamp     = timestamp
        self.machineType   = machineType
        self.url           = url

    def __str__(self):
        return '''
%s
current      : %s
current_alert: %s
location     : %s
name         : %s
state        : %s
timestamp    : %s
machineType  : %s
''' % (self.__class__,self.current, self.current_alert, self.location, \
        self.name, self.state, self.timestamp, self.machineType)

    def update(self,machine):
        ''' update machine attributes from dictionary '''
        publickeys =(key for key in dir(machine) if not key.startswith('__'))

        for key in publickeys:
            setattr(self,key,getattr(machine,key))


class MachineBuffered(Machine):
    ''' Machine with current buffer '''

    def __init__(self, *args, **kwargs):
        '''
        param: bufferTimedelta - max timedelta [s] of the buffer (positive number)
        '''
        self.__currentBuffer = [] # holds list of current values with timestamp (<int>,<datetime>)
        self.bufferTimeframe = datetime.timedelta(seconds=abs(kwargs['bufferTimedelta']))
        del kwargs['bufferTimedelta']
        Machine.__init__(self,*args,**kwargs)


    def __str__(self):
        return super(MachineBuffered,self).__str__() + \
                "currentBuffer %s\ncurrentAverage: %s" % \
                (len(self.__currentBuffer), self.currentAvg())


    def addCurrent(self, value, timestamp):
        for entry in self.__currentBuffer[:]:
            if (timestamp - entry['timestamp']) > self.bufferTimeframe:
                self.__currentBuffer.remove(entry)

        # add, if new entry does not exists
        self.__currentBuffer.append({'current':value,'timestamp':timestamp})


    def currentAvg(self):
        '''return average current of the buffer'''

        if not len(self.__currentBuffer): return None
        a = sum(map(lambda x: x['current'],self.__currentBuffer))
        avg = a/len(self.__currentBuffer)
        return avg


