#!/usr/bin/env python
"""
    actyx.monitor
    ~~~~~~~~~~~~~

    :copyright: (c) 2015 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: machine monitors
"""


import time
import logging
import os
from multiprocessing import Process

class Watchdog(Process):
    ''' The watchdog continuously fetches the machine status from 
    the apihandler and updates its subscibers '''


    def __init__(self, apiHandler, machine, listeners=None):
        ''' init monitor on apiHandler for machine'''

        Process.__init__(self)

        self.apiHandler = apiHandler
        self.machine    = machine
        self.listeners  = listeners or []
        self.shutdown   = False

        self.start()


    def __str__(self):
        return "classname : %s\nprocess id: %i" % (self.__class__,os.getpid())


    def kill(self):
        ''' stop monitor '''
        self.__shutdown = True


    def subscribe(self,listener):
        '''add MonitorListener'''

        if listener not in self.listeners:
            self.listeners.append(listener)


    def unsubscribe(self,listener):
        '''remove MonitorListener'''
        self.listeners.remove(listener)


class MachineParkWatchdog(Watchdog):
    ''' The watchdog checks the machine park and informs its listeners about
    changes in the set of machines.
    '''

    def __init__(self, apiHandler, listeners=None):
        ''' init monitor on apiHandler for machine'''
        Watchdoc.__init(self, apiHandler, None, listeners)


    def run(self):
        ''' inf loop, see class definition'''
        
        machinepark = []
        while(not self.shutdown):
            time.sleep(1)
            try:
                machineparkUpdate = self.apiHandler.getMachines()
                if set(machinepark) != set(machineparkUpdate):
                    for listener in self.listeners:
                        listener.update(machineparkUpdate)
                machinepark = machineparkUpdate
            except:
                logging.error("error updating machine")
                pass


class MaxCurrentWatchdog(Watchdog):
    ''' The watchdog continuously fetches the current machine status from 
    the handler and updates its subscibers if the current is higher
    than current alert
    '''

    def run(self):
        ''' inf loop, see class definition'''
        while(not self.shutdown):
            time.sleep(5)
            m = self.apiHandler.getMachine(self.machine.url)
            self.machine.update(m)
            self.machine.addCurrent(int(m.current),m.timestamp)
            if self.machine.current > self.machine.current_alert:
                msg = "Max current exceeded at machine: %s" % self.machine
                for listener in self.listeners:
                    listener.update(msg)


class MachineEnvWatchdog(Watchdog):
    ''' The watchdog continuously fetches the machine status and
    the environment status and updated its subscribers
    '''

    def run(self):
        ''' inf loop, see class definition'''
        while(not self.shutdown):
            time.sleep(1)
            e = self.apiHandler.getEnvironment()
            m = self.apiHandler.getMachine(self.machine.url)
            msg = { 'machine' : m,
                    'env'     : e
                   }
            for listener in self.listeners:
                listener.update(msg)

