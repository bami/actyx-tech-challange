#!/usr/bin/env python
"""
    actyx.listeners
    ~~~~~~~~~~~~~~~

    :copyright: (c) 2015 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: machine monitors
"""

from scipy import stats

class WatchdogListener(object):

    def __init__(self, name=None):
        self.name = name or 'anonymous'

    def update(self, msg):
        ''' callback '''
        pass


class ConsoleListener(WatchdogListener):
    ''' WatchdogListener that logs the monitor events to the console'''


    def __init__(self, name=None):
        super(ConsoleListener,self).__init__(name=name)
        print(':: % s : Waiting for machine machine events ...' % self.name)


    def update(self, msg):
        ''' listener callback: print machine state to console '''
        print(":: %s : %s" % (self.name,msg))


class BufferListener(WatchdogListener):
    ''' WatchdogListener that logs the monitor events to the console'''

    def __init__(self, maxSize, name=None):
        super(BufferListener,self).__init__(name=name)

        self.maxSize = maxSize
        self.buffer = []
        pass


    def update(self, msg):
        ''' listener callback: print machine state to console '''

        super(BufferListener,self).update(msg)

        if len(self.buffer) >= self.maxSize:
            self.buffer.pop(0)

        self.buffer.append(msg)


class CorelationAnalysisListener(BufferListener):
    ''' Listener that calculated the correlation between machine current
    and environment data'''

    def __init__(self, maxSize, name=None):
        super(CorelationAnalysisListener,self).__init__(maxSize=maxSize, name=name)
        print(':: % s : Waiting for machine machine events ...' % self.name)


    def update(self, msg):
        super(CorelationAnalysisListener,self).update(msg)

        current     = [c['machine'].current for c in self.buffer]
        temperature = [p['env']['temperature'][1] for p in self.buffer]
        humidity    = [p['env']['humidity'][1] for p in self.buffer]
        pressure    = [p['env']['pressure'][1] for p in self.buffer]

        nSamples = len(current)

        # regression analysis
        try:
            slope, intercept, r_value, p_value, std_err = stats.linregress(current,temperature)
            print("%s: current/temp corelation p-value: %s (%i samples)" % (self.name,p_value, nSamples))

            slope, intercept, r_value, p_value, std_err = stats.linregress(current,humidity)
            print("%s: current/humidity corelation p-value: %s (%i samples)" % (self.name,p_value,nSamples))

            slope, intercept, r_value, p_value, std_err = stats.linregress(current,pressure)
            print("%s: current/pressure corelation p-value: %s (%i samples)" % (self.name,p_value,nSamples))
        except:
            logging.error("error in correlation analysis")


