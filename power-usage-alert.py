#!/usr/bin/env python
"""
    power_usage_alert
    ~~~~~~~~~~~~~~~~~

    :copyright: (c) 2015 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: main application as starting point
"""


import logging
import os
import signal
from actyx.api import ApiHandler
from actyx.monitor import MachineParkWatchdog, MaxCurrentWatchdog
from actyx.listener import ConsoleListener
from actyx.listener import WatchdogListener

machinePark = []
watcherPark = []

def signalHandler(signum, frame):
    if signum in (signal.SIGTERM,signal.SIGINT):
        shutdown()


def main():
    ''' init and run system '''

    logging.info("init machine park")
    # init machine park
    global machinePark, watcherPark
    ah = ApiHandler()
    mUrls = ah.getMachines()
    
    # set up machine park and register watchdogs
    logging.debug("register watcher")
    for mUrl in mUrls[:2]: 
        machine = ah.getMachine(mUrl)
        machinePark.append(machine)

        watcher = MaxCurrentWatchdog(ApiHandler(),machine)  
        watcher.subscribe(ConsoleListener(name=machine.name))
        watcherPark.append(watcher)


def shutdown():
    ''' shutdown system '''
    logging.info("shut down machine park")
    global watcherPark
    for watcher in watcherPark: watcher.kill()
    if __name__ == '__main__': exit()

    
if __name__ == '__main__':
    logging.basicConfig(filename='monitor.log',level=logging.INFO)
    print("pid: %i" % os.getpid())
    signal.signal(signal.SIGTERM, signalHandler)
    main()
