#!/usr/bin/env python
"""
    current-env-regression
    ~~~~~~~~~~~~~~~~~~~~~~

    :copyright: (c) 2015 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: main application as starting point
"""


import logging
from actyx.api import ApiHandler
from actyx.monitor import MachineEnvWatchdog
from actyx.listener import CorelationAnalysisListener

machinePark = []

def main():

    ah = ApiHandler()
    global machinePark

    mUrls = ah.getMachines()

    # set up machine park and register watchdogs
    for mUrl in mUrls: 
        machine = ah.getMachine(mUrl)
        machinePark.append(machine)
        m = MachineEnvWatchdog(ApiHandler(),machine)  
        m.subscribe(CorelationAnalysisListener(maxSize=42,name=machine.name))


if __name__ == '__main__':
    logging.basicConfig(filename='monitor.log',level=logging.INFO)
    main()
