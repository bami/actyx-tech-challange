#!/usr/bin/env python
"""
    test.test_models
    ~~~~~~~~~~~~~~~~

    :copyright: (c) 2015 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: unit test of models
"""


import unittest
import random
import string
from actyx.models import Machine
class MachineTest(unittest.TestCase):

    def randString(length):
        return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(length))

    def test_initmachineattributes(self):

        current       = random.randint(1,42)
        current_alert = random.randint(1,42)
        location      = MachineTest.randString(42)
        name          = MachineTest.randString(42)
        url           = MachineTest.randString(42)

        machine = Machine(current=current,
                current_alert=current_alert,
                location = location,
                name = name,
                url = url)
        
        self.assertEqual(machine.current, current)
        self.assertEqual(machine.current_alert, current_alert)
        self.assertEqual(machine.location, location)
        self.assertEqual(machine.name, name)
        self.assertEqual(machine.url, url)

if __name__ == '__main__':
    unittest.main()
